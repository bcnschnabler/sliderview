//
//  SliderView.swift
//  Steps
//
//  Created by Tobias Dorner on 22.03.22.
//

import SwiftUI

struct SliderView: View {
    init(currentValue: Binding<Int>, steps: Int, width: CGFloat) {
        self.width = width
        self.steps = steps
        self.currentValue = currentValue
    }
    
    @State private var sliderLocation: CGPoint = CGPoint(x: 0.0, y: 50)
    
    /// FIX: Needs to be a binding
    var currentValue: Binding<Int>
    
    /// Calculated property
    @State private var snapToOffset: Float = 0.0
    
    /// Used for thumbcolor
    @State private var isInitialState: Bool = true
    
    /// Used for checkmark/thumbimage
    @State private var isDragging: Bool = false
    
    /// Number of intervals, does not work even when it's not 10. Also, it's 0-10 steps, so 11. Use range.
    var steps: Int
    
    /// Total width of the view. Can be done better, aka Geometry reader.
    var width: CGFloat
    
    /// Slider/View customization
    private var activeSliderColor = Color(UIColor.systemYellow)
    private var inactiveSliderColor = Color(UIColor.systemGray5)
    private var sliderThumbColor = Color(UIColor.systemOrange)
    private var initialSliderThumbColor = Color(UIColor.systemYellow)
    private var activeNumberIndictorColor = Color(UIColor.systemOrange)
    private var activeRectangleIndictorColor = Color(UIColor.systemYellow)
    private var inactiveNumberIndictorColor = Color(UIColor.systemGray2)
    private var backgroundColor: Color = Color(red: 1.0, green: 1.0, blue: 0.9)
    private var thumbImage = Image(systemName: "checkmark")

    var body: some View {
        ZStack {
            backgroundColor.ignoresSafeArea()
            VStack(spacing: -30.0) {
                HStack(spacing: 0.0) {
                    ForEach(0...steps, id: \.self) { index in
                        HStack {
                            VStack(alignment: .center, spacing: 5.0) {
                                Text("\(index)")
                                    .font(
                                        index == currentValue.wrappedValue ? .body.weight(.semibold) : .callout.weight(.semibold)
                                    )
                                    .foregroundColor(
                                        (index <= currentValue.wrappedValue) && !isInitialState ? activeNumberIndictorColor : inactiveNumberIndictorColor
                                    )
                                Rectangle().frame(width: 1, height: 8.0, alignment: .leading)
                                    .foregroundColor(
                                        index <= currentValue.wrappedValue ? activeRectangleIndictorColor : inactiveNumberIndictorColor
                                    )
                                    .opacity(
                                        index == currentValue.wrappedValue ? 0.0 : 1.0
                                    )
                            }
                        }
                        .frame(width: width / Double(steps + 1))
                        .animation(.interactiveSpring(response: 0.1,
                                                      dampingFraction: 0.4,
                                                      blendDuration: 1.0), value: self.currentValue.wrappedValue)
                        .transition(AnyTransition.scale.animation(.default))
                        .offset(y: (index == currentValue.wrappedValue) && !isInitialState ? 0 : 5.0)
                    }
                }
                .frame(width: width, height: 20.0, alignment: .leading)
                ZStack {
                    RoundedRectangle(cornerRadius: 12)
                        .frame(width: width, height: 8, alignment: .leading)
                        .foregroundColor(inactiveSliderColor)
                    HStack {
                        RoundedRectangle(cornerRadius: 12)
                            .frame(width: max(0.0, sliderLocation.x), height: 8, alignment: .leading)
                            .foregroundColor(activeSliderColor)
                        Spacer()
                    }
                    ZStack {
                        Circle()
                            .frame(width: 35)
                            .foregroundColor(isInitialState ? initialSliderThumbColor : sliderThumbColor)
                            .animation(.interactiveSpring(response: 0.1,
                                                          dampingFraction: 0.4,
                                                          blendDuration: 1.0), value: self.sliderLocation.x)
                        thumbImage
                            .resizable()
                            .font(Font.title.weight(.semibold))
                            .frame(width: 12, height: 12)
                            .foregroundColor(.white)
                            .opacity(isDragging ? 0.0 : 1.0)
                    }
                    .frame(width: 50, height: 50)
                    .position(sliderLocation)
                    .gesture(
                        DragGesture()
                            .onChanged { value in
                                /// Discard inital state
                                self.isInitialState = false
                                
                                /// Indicate we're dragging
                                self.isDragging = true
                                
                                /// Calculate slider location
                                self.sliderLocation.x = self.calculateSliderLocation(for: value)
                                
                                /// Calculate current value (int)
                                self.calculateCurrentValue(for: value)
                            }
                            .onEnded { value in
                                self.isDragging = false
                                self.snapToNearestOffset()
                                self.hapticEnded()
                            }
                    )
                }
                .frame(width: width, height: 100.0, alignment: .leading)
            }
        }.onAppear {
            self.snapToNearestOffset()
            
            /// This way we can feed an initial value from the outside
            if self.currentValue.wrappedValue > 0 {
                self.isInitialState = false
            }
        }
    }
                        
    func calculateCurrentValue(for value: DragGesture.Value) {
        /// Here we are creating a "normalized" value from 1 to 10.
        let normalized: Double = value.location.x / (width * Double(steps)) * 100.0
        /// We round the value, so we can pass it as an int
        var rounded = Int(round(normalized))
        
        /// Ugh. min/max. Limit current value to value in between 0 and number of steps.
        if rounded > steps {
            rounded = steps
        } else if rounded < 0 {
            rounded = 0
        }
        
        /// We check our stored value, and if it changed, we give haptic feedback
        if self.currentValue.wrappedValue != rounded {
            self.currentValue.wrappedValue = rounded
            hapticIncrement()
        }
    }
    
    func snapToNearestOffset() {
        /// Size of an item + half of it
        let offset = (itemWidth * CGFloat(currentValue.wrappedValue)) + (itemWidth / 2.0)
                
        self.sliderLocation.x = offset
        self.snapToOffset = Float(offset)
    }
    
    /// Here we limit the slider horizontally
    func calculateSliderLocation(for value: DragGesture.Value) -> CGFloat {
        if value.location.x < 0.0 {
            return 0.0
        }
        
        if value.location.x > self.width{
            return self.width
        }
        
        return value.location.x
    }
    
    private var itemWidth: Double {
        return width / Double(steps + 1)
    }

    private func hapticIncrement() {
        let generator = UIImpactFeedbackGenerator(style: .heavy)
        generator.impactOccurred()
    }

    private func hapticEnded() {
        let generator = UINotificationFeedbackGenerator()
        generator.notificationOccurred(.success)
    }
}

struct SliderView_Previews: PreviewProvider {
    @State static var value: Int = 2
    
    static var previews: some View {
        SliderView(currentValue: Self.$value, steps: 10, width: 300.0)
    }
}
