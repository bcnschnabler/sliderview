//
//  SliderApp.swift
//  Slider
//
//  Created by Tobias Dorner on 22.03.22.
//

import SwiftUI

@main
struct SliderApp: App {
    @State var value: Int = 0
    
    var body: some Scene {
        WindowGroup {
            SliderView(currentValue: $value, steps: 10, width: 300.0)
        }
    }
}
